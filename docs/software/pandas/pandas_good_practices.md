# Good practices in Pandas dataframes

A collection of examples on good practices in Panda DataFrame analysis. Examples reconstructed from Pandas online documentation and search in Web pages. 


## Create new columns in a dataframe

There are two ways to create a new column in a Pandas dataframe. 


```python
import pandas as pd
import numpy as np

df = pd.DataFrame({'A': [x for x in np.arange(0, 10)]})

# - create a new column with direct assignment. The bad practice! 
df['B'] = df['A']**2

# - use t
```

with the assign function of Pandas, we can create single or multiple columns and even use them directly. 



```python
df.assign(
    alfa_squared = lambda row: row['A']**2,
    alfa_halfed = lambda row : row['A']/2,
    alfa_sq_halfed = lambda row : row['alfa_squared']/2
    )
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>A</th>
      <th>B</th>
      <th>alfa_squared</th>
      <th>alfa_halfed</th>
      <th>alfa_sq_halfed</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>0.5</td>
      <td>0.5</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>1.0</td>
      <td>2.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>3</td>
      <td>9</td>
      <td>9</td>
      <td>1.5</td>
      <td>4.5</td>
    </tr>
    <tr>
      <th>4</th>
      <td>4</td>
      <td>16</td>
      <td>16</td>
      <td>2.0</td>
      <td>8.0</td>
    </tr>
    <tr>
      <th>5</th>
      <td>5</td>
      <td>25</td>
      <td>25</td>
      <td>2.5</td>
      <td>12.5</td>
    </tr>
    <tr>
      <th>6</th>
      <td>6</td>
      <td>36</td>
      <td>36</td>
      <td>3.0</td>
      <td>18.0</td>
    </tr>
    <tr>
      <th>7</th>
      <td>7</td>
      <td>49</td>
      <td>49</td>
      <td>3.5</td>
      <td>24.5</td>
    </tr>
    <tr>
      <th>8</th>
      <td>8</td>
      <td>64</td>
      <td>64</td>
      <td>4.0</td>
      <td>32.0</td>
    </tr>
    <tr>
      <th>9</th>
      <td>9</td>
      <td>81</td>
      <td>81</td>
      <td>4.5</td>
      <td>40.5</td>
    </tr>
  </tbody>
</table>
</div>



The creation of the new column can be combined with a selection of hte original df


```python
df.assign(
    beta_squared = lambda row: row['B']**2
    ).loc[lambda row : row['A'] > 3]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>A</th>
      <th>B</th>
      <th>beta_squared</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>4</th>
      <td>4</td>
      <td>16</td>
      <td>256</td>
    </tr>
    <tr>
      <th>5</th>
      <td>5</td>
      <td>25</td>
      <td>625</td>
    </tr>
    <tr>
      <th>6</th>
      <td>6</td>
      <td>36</td>
      <td>1296</td>
    </tr>
    <tr>
      <th>7</th>
      <td>7</td>
      <td>49</td>
      <td>2401</td>
    </tr>
    <tr>
      <th>8</th>
      <td>8</td>
      <td>64</td>
      <td>4096</td>
    </tr>
    <tr>
      <th>9</th>
      <td>9</td>
      <td>81</td>
      <td>6561</td>
    </tr>
  </tbody>
</table>
</div>



## Using internal panda functions in aggregation


```python
import numpy as np

data = np.random.randint(0, 1000, size=300)
df = pd.DataFrame(data, columns=['data'])
df = df.assign(
        decade = lambda row: (row['data']/10).astype(int)
    )
```


```python
df.groupby('decade').mean()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>data</th>
    </tr>
    <tr>
      <th>decade</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>7.600000</td>
    </tr>
    <tr>
      <th>1</th>
      <td>14.750000</td>
    </tr>
    <tr>
      <th>2</th>
      <td>22.200000</td>
    </tr>
    <tr>
      <th>3</th>
      <td>36.500000</td>
    </tr>
    <tr>
      <th>4</th>
      <td>49.000000</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
    </tr>
    <tr>
      <th>95</th>
      <td>954.333333</td>
    </tr>
    <tr>
      <th>96</th>
      <td>964.166667</td>
    </tr>
    <tr>
      <th>97</th>
      <td>974.500000</td>
    </tr>
    <tr>
      <th>98</th>
      <td>980.333333</td>
    </tr>
    <tr>
      <th>99</th>
      <td>995.750000</td>
    </tr>
  </tbody>
</table>
<p>98 rows × 1 columns</p>
</div>




```python
df = df.groupby('decade').agg(
                decade_mean = pd.NamedAgg(column='data', aggfunc='mean'),
                decade_std = pd.NamedAgg(column='data', aggfunc='std'),
                decade_npstd = pd.NamedAgg(column='data', aggfunc=np.std)
            )
df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>decade_mean</th>
      <th>decade_std</th>
      <th>decade_npstd</th>
    </tr>
    <tr>
      <th>decade</th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>7.600000</td>
      <td>1.140175</td>
      <td>1.140175</td>
    </tr>
    <tr>
      <th>1</th>
      <td>14.750000</td>
      <td>3.774917</td>
      <td>3.774917</td>
    </tr>
    <tr>
      <th>2</th>
      <td>22.200000</td>
      <td>2.167948</td>
      <td>2.167948</td>
    </tr>
    <tr>
      <th>3</th>
      <td>36.500000</td>
      <td>4.358899</td>
      <td>4.358899</td>
    </tr>
    <tr>
      <th>4</th>
      <td>49.000000</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>95</th>
      <td>954.333333</td>
      <td>2.081666</td>
      <td>2.081666</td>
    </tr>
    <tr>
      <th>96</th>
      <td>964.166667</td>
      <td>3.125167</td>
      <td>3.125167</td>
    </tr>
    <tr>
      <th>97</th>
      <td>974.500000</td>
      <td>0.707107</td>
      <td>0.707107</td>
    </tr>
    <tr>
      <th>98</th>
      <td>980.333333</td>
      <td>0.577350</td>
      <td>0.577350</td>
    </tr>
    <tr>
      <th>99</th>
      <td>995.750000</td>
      <td>3.201562</td>
      <td>3.201562</td>
    </tr>
  </tbody>
</table>
<p>98 rows × 3 columns</p>
</div>




```python
xpd = df['decade_std'].values
xnp = df['decade_npstd'].values
xnp-xpd
```




    array([ 0.,  0.,  0.,  0., nan,  0.,  0., nan,  0.,  0.,  0.,  0.,  0.,
            0., nan,  0.,  0., nan,  0., nan,  0.,  0.,  0.,  0.,  0.,  0.,
            0., nan,  0.,  0.,  0.,  0., nan,  0.,  0.,  0., nan,  0.,  0.,
            0.,  0.,  0.,  0., nan,  0.,  0.,  0.,  0.,  0.,  0., nan,  0.,
            0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0., nan,  0.,
            0.,  0., nan,  0.,  0.,  0.,  0.,  0., nan,  0., nan,  0.,  0.,
            0.,  0.,  0., nan,  0.,  0., nan,  0.,  0.,  0.,  0.,  0.,  0.,
           nan,  0.,  0.,  0.,  0.,  0.,  0.])




```python

```
