## Create an md from Jupyter Notebook
in SWAN, just save the file as .md. 
**Note** : to display correctly the TOC each file must have only one h1 definition.  


## Contribute to a Git Project

I found this [page]([url](https://www.dataschool.io/how-to-contribute-on-github/)) that describes a clear recipie on the steps to follow if you wish to rontribute to an open source project. 

## Panda DataFrame tips
My collection of tips for doing analysis with pandas dataframes - [pandas tips](howtopandas.md)

## Python coding tips

My collection of tips for data analysis with python - [python tips](howtopython.md)
