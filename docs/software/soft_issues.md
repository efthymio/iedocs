## Cpymad and MAD-X 

My findings from working with cpymad and MAD-X 

### cpymad fails to activate the "beam,radiate" command.

In MAD-X one can use the command 
```
beam,radiate;
```
to activate the evaluation in a subsequent ```twiss``` command the evaluation of the radiation integrals. Assuming of course there is some RF installed and activated in the sequence. 
The file : [ForGuido_test_ibs.madx](https://gitlab.cern.ch/efthymio/iedocs/-/blob/master/docs/software/cpymad/ForGuido_test_ibs.madx) is a pure MAD-X scripts that uses this feature.

Can be


