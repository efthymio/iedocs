# Using Git

For a guick reference guide for daily Git look at this [blog](https://www.dataschool.io/git-quick-reference-for-beginners/) It contains a tutorial for Git beginners. Follow references therein. In particular the **Git Cheatsheet** link. 

## Git commands to note

Some commands to help you exploit the full power of git to manage your repository. With reference to this [blog](https://towardsdatascience.com/10-git-commands-you-should-know-df54bea1595c)

* **git diff** : see the difference of local files to remote
* **git log**  : inspect the log of commits - history of 
* **git blame _<my_file>_** : see who changed what and when in _<my_file>_
* **git reflog** : show a log of changes to the local repository HEAD. Can show lost work done locally.

* **git reset --hard HEAD** - discard staged and unstaged changes since the most recent commits
* **git checkout _my_commit_** : discard unstaged changes since _my_commit_
    * _git checkout HEAD_ : to discard changes since the most recent commit
    * _git cehckout _branch_ 

* **git revert _my_commit_**:  undo the effects of changed in _my_commit_
* **git clean -n** : delete untrakced fiels in the local working directory
    * flags to consider: -n -f -d

* **git commit --amend** : add your staged changes to the most recetn commit

* **git push _my_remote_ --tags** : send all local tags to the remote repository.

* **git branch** : list local branches, option **-r** to list remote branches

* **git checkout <local_branch>** : switch to local branch 

* **git diff <local_branch> origin/master** : difference of local branch to master

* **git branch --merged/no-merded** : list branches that have been merged to HEAD

* **git branc -d <local_branch>** : will delete a local branch if was already merged to the master! 

