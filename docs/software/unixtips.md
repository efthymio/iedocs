# Unix and Mac Tips

## EOS 

* check ownership of a project volume : 
    ```
    lxplus > eos root://eosproject.cern.ch ls -lah /eos/project/l/lhc-lumimod
    ```
See here for more details on [managing CERNBOX user and project space](https://cernbox.docs.cern.ch/web/quota/) 

## Unix commands

* modify last line of a file : ```sed '$s/python/#python` test.txt > test1.txt ```
* update the date of all file in a directory tree : ``` find . -exec touch {}\;```
* gtar files excluding files and remove absolute path: ```gtar -zcvf test.tar.gz —exclude=‘*.svn*’ -C ~/rep-eng/ps/cps. NEW```
* suppress terminal colors : ```\ls -ltr``` or ```\ls```
