# Python treasures from the internet 

My collection of interesting posts on python.

- **colored logs** : introduce [colors in log files](https://medium.com/geekculture/colored-logs-for-python-2973935a9b02)


## Local and global variables in functions

Using global variables to local functions can be dangerous! 
Example the code below:

```
beta = 10
alfa = list(np.arange(1, 5))
for j in np.arange(0, 3):
    def xx(j, beta, alfa):
        beta = beta * (1+j*1e-4)
        alfa[1] = 1232342.+j
        print(f'     inside     : {j=} {beta=:<12.3f}, {alfa=}')
    print(f' in the main    : {j=} {beta=:<12.3f}, {alfa=}')
    xx(j, beta, alfa)
    print(f' >> final : {beta=:<12.3f}, {alfa=}')

```
which produces this output: 
```
    in the main    : j=0 beta=10.000      , alfa=[1, 2, 3, 4]
        inside     : j=0 beta=10.000      , alfa=[1, 1232342.0, 3, 4]
    >> final : beta=10.000      , alfa=[1, 1232342.0, 3, 4]
    in the main    : j=1 beta=10.000      , alfa=[1, 1232342.0, 3, 4]
        inside     : j=1 beta=10.001      , alfa=[1, 1232343.0, 3, 4]
    >> final : beta=10.000      , alfa=[1, 1232343.0, 3, 4]
    in the main    : j=2 beta=10.000      , alfa=[1, 1232343.0, 3, 4]
        inside     : j=2 beta=10.002      , alfa=[1, 1232344.0, 3, 4]
    >> final : beta=10.000      , alfa=[1, 1232344.0, 3, 4]
```
So, the variable ```beta``` is used inside the function but is not modified outside its scope, while the object ```alfa``` is modified for the whole program. 

A good reading on the issue [here](https://realpython.com/python-pass-by-reference/)
