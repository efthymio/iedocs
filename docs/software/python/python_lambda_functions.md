# Dictionaries and lambda functions

How to use lambda functions to sort, or apply functions to dictionaries


```python
mydict={'a':4, 'bright':1, 'day':0, 'starts': 10, 'with':-2, 'kisses': 13}
```


```python
mydict.items()
```




    dict_items([('a', 4), ('bright', 1), ('day', 0), ('starts', 10), ('with', -2), ('kisses', 13)])



## Sort using keys or its values


```python
sorted(mydict.items(), key=lambda item : item[0])
```




    [('a', 4),
     ('bright', 1),
     ('day', 0),
     ('kisses', 13),
     ('starts', 10),
     ('with', -2)]




```python
sorted(mydict.items(), key=lambda item : item[1])
```




    [('with', -2),
     ('day', 0),
     ('bright', 1),
     ('a', 4),
     ('starts', 10),
     ('kisses', 13)]



### Apply other functions using lambdas


```python
min(mydict.items(), key=lambda item : item[0])
```




    ('a', 4)




```python
min(mydict.items(), key=lambda item : item[1])
```




    ('with', -2)




```python
max(mydict.items(), key=lambda item : item[0])
```




    ('with', -2)



we can also apply multiple conditions


```python
sorted(mydict.items(), key=lambda item : (item[1]))
```




    [('with', -2),
     ('day', 0),
     ('bright', 1),
     ('a', 4),
     ('starts', 10),
     ('kisses', 13)]




```python
l = sorted(mydict.items(), key=lambda item : (item[1], len(item[0])))
mynewdict = {k:v for k, v in l}
mynewdict
```




    {'with': -2, 'day': 0, 'bright': 1, 'a': 4, 'starts': 10, 'kisses': 13}




```python

```
