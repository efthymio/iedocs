# FCC-ee Simulations

For the background studies we use the GuineaPig program

## Input file

A sample input file is avaialble [here](./FCCee/acc_ref.dat) provided by E. Perez (EP)
Below some information on the file contents and parameters. For the pameters I use the information in the [documentation file of the gitlab repository](https://gitlab.cern.ch/clic-software/guinea-pig/-/blob/master/doc/GuineaPigManual.pdf)

### Accelerators

- **FCCee** : that corresponds to the operation at Z-pole

    ```
    $ACCELERATOR:: FCCee
    {
    energy=45.6;        beam energy in [GeV]
    particles=17.0;     particles per bunch [1e10]
    beta_x=150;         beam beta x [mm]
    beta_y=0.8;         beam beta y [mm]
    sigma_x=6360;       horiz beam size [nm]
    sigma_y=28.3;       vert beam size [nm]
    sigma_z=12100;      long beam size [um]
    dist_z.1=0;         long charge beam-1 [normal distr]
    dist_z.2=0;         long charge beam-2 [norma distr]
    n_b=1;              number of bunches per train [unused]
    f_rep=1;            rev frequenct [unused, results per bunch]
    angle_x=-0.015;     horiz half crossing angle [rad]
    }
    ```

- **FCCee-Top** : that corresponds to the operation at t-pole

    ```
    $ACCELERATOR:: FCCee_Top
    {
    energy=182.5;        beam energy in [GeV]
    particles=23;     particles per bunch [1e10]
    beta_x=1000;         beam beta x [mm]
    beta_y=1.6;         beam beta y [mm]
    sigma_x=38210;       horiz beam size [nm]
    sigma_y=68;       vert beam size [nm]
    sigma_z=2540;      long beam size [um]
    dist_z.1=0;         long charge beam-1 [normal distr]
    dist_z.2=0;         long charge beam-2 [norma distr]
    n_b=1;              number of bunches per train [unused]
    f_rep=1;            rev frequenct [unused, results per bunch]
    angle_x=-0.015;     horiz half crossing angle [rad]
    }
    ```
    Default values in the program 
    ```
    which_sepread = 0;  no energy spread in the beam
    charge_signe = -1;  relative sign of charge of the two beams, i.e. (e+e-) collider
    offset_x = 0;       no horizontal offset [nm]
    offset_y = 0;       no vertical offset [nm]
    waist_x = 0;        no horizontal waist shift [um]
    waist_y = 0;        no vertical waist shift [um]
    angle_y = 0;
    angle_phi = 0;
    ```

### Parameters

- **FCCee_pairs** : to generate pair electrons and background photons
    <details><summary markdown="span"> expand</summary>

    ```
    $PARAMETERS:: FCCee_pairs
    {
    n_x=64*25 ;             no cells in x-direction        
    n_y=64*10 ;             no cells in y-direction
    n_z=150 ;               no cells in z-direction
    n_t=1 ;                 no timesteps to move the slices
    cut_x=150*sigma_x.1;    grid size in x-direction [nm]
    cut_y=60.0*sigma_y.1;   grid size in y-direction [nm]
    cut_z=3.0*sigma_z.1 ;   grid size in long direction [um]
    n_m= 50000;             number of macro-particles
    electron_ratio=1;       all electrons for bckgnd calculations 
    charge_sign=-1.;
    store_beam=0;           no not store spent beam      
    do_photons=1;           store the generated bremstrahlung photons (photons.dat file)
    do_pairs=1;             store the incoherent pair products (pairs.dat)
    do_lumi = 1;            store the luminosity info (lumi.ee.out)
    track_pairs=1;          track pairs in the field of the beams
    store_pairs=2;          ???
    pair_q2 = 2 ;           Q2(=s/4) scale for the pair production
    pair_ratio = 1.;        store all generated pairs
    grids=7 ;               grid to use for pair tracking
    rndm_seed=1;            
    }
    ```
    <p>
    </details>


- **FCCee_BS_for_test*** :
    <details><summary> expand </summary>

    ```
    $PARAMETERS:: FCCee_BS_for_test
    {
    n_x=64*25 ;
    n_y=64*10 ;
    n_z=5 ;
    n_t=1 ;
    cut_x=150*sigma_x.1;
    cut_y=60.0*sigma_y.1;
    cut_z=3.0*sigma_z.1 ;
    n_m= 50000;
    electron_ratio=1;
    store_beam=1;             store the spent beam
    do_photons=1;
    store_photons=1;
    do_pairs=0;
    track_pairs=0;
    store_pairs=2;
    pair_q2 = 2 ;
    pair_ratio = 1.;        
    do_lumi=0 ;
    num_lumi=200000 ;       number of scatters to store in lumi.dat
    lumi_p=1e-28;           scaling factor for stored particles in lumi.dat
    do_bhabhas=0;           do not produce bhabha pairs
    bhabha_scal=1e-29;
    bhabha_ecmload=91.2;    
    rndm_save=1;
    rndm_load=0;
    load_events=0;          
    grids=1 ;               
    do_dump=0;              
    rndm_seed=1;
    }
    ```
    </details>

- ***FCCee_BS*** : background studies 
    <details><summary> expand </summary>

    ```
    $PARAMETERS:: FCCee_BS
    {
    n_x=64*25 ;
    n_y=64*10 ;
    n_z=300 ;
    n_t=1 ;
    cut_x=150*sigma_x.1;
    cut_y=60.0*sigma_y.1;
    cut_z=3.0*sigma_z.1 ;
    n_m= 50000;
    electron_ratio=1;
    store_beam=1;
    do_photons=1;
    store_photons=1;
    do_pairs=0;
    track_pairs=0;
    store_pairs=2;
    pair_q2 = 2 ;
    pair_ratio = 1.;
    do_lumi=0 ;
    num_lumi=200000 ;
    lumi_p=1e-28;
    do_bhabhas=0;
    bhabha_scal=1e-29;
    bhabha_ecmload=91.2;
    rndm_save=1;
    rndm_load=0;
    load_events=0;
    grids=1 ;
    do_dump=0;
    rndm_seed=1;
    }
    ```
    </details>

- ***FCCee_BS_Top*** : 
    <details><summary> expand </summary>

    ```
    $PARAMETERS:: FCCee_BS_Top
    {
    n_x=128;
    n_y=128 ;
    n_z=200 ;
    n_t=1 ;
    cut_x=12*sigma_x.1;
    cut_y=10.0*sigma_y.1;
    cut_z=3.0*sigma_z.1 ;
    n_m= 50000;
    electron_ratio=1;
    store_beam=1;
    do_photons=1;
    store_photons=1;
    do_pairs=0;
    track_pairs=0;
    store_pairs=2;
    pair_q2 = 2 ;
    pair_ratio = 1.;
    do_lumi=0 ;
    num_lumi=200000 ;
    lumi_p=1e-28;
    do_bhabhas=0;
    bhabha_scal=1e-29;
    bhabha_ecmload=91.2;
    rndm_save=1;
    rndm_load=0;
    load_events=0;
    grids=1 ;
    do_dump=0;
    rndm_seed=1;
    }
    ```
    </details>
