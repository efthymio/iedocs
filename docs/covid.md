# My Collection related to COVID-19  
 

Web pages with statistics and information  

--------------------------------------------

*   Hamilton company - [web](https://www.hamiltoncompany.com/about-us)  
    
*   Global statistics -  [CSSE site at John Hopkins University (JHU)](https://www.arcgis.com/apps/opsdashboard/index.html#/bda7594740fd40299423467b48e9ecf6)  
    
*   Echinaforce - Forte - [web](https://www.greenweez.com/a-vogel-echinaforce-famille-x-400-comprimes-p105452?gclid=CjwKCAjwsMzzBRACEiwAx4lLGzVQUMf35hDmqIOFmkBcz0MGgMXKlU-rh9ll8QsFqL2Negvm6PH7DBoC_twQAvD_BwE)  
    
*   Statistics - [www.shinyapps.org](http://shinyapps.org/apps/corona/)  
    
*   Genomic evolution - [www.nextstrain.org](https://nextstrain.org./ncov?f_country=France%2cGermany%2cItaly%2cSwitzerland%2cUSA%2cUnited%20Kingdom)  
    
*   Detailed info on Switzerland - [www.corona-data.ch](http://www.corona-data.ch/)  
    
*   ECDC data [web](https://www.ecdc.europa.eu/en/covid-19/country-overviews)  
    

  

*   ANFOR organization [web](https://www.afnor.org/en/)  
    

>   

Scientific documents and presentations
--------------------------------------

*   Michal Caspi Tal, PhD Stanford University School of Medicine, Coronavirus for non virologists, [March 9, 2020](/efthymio/Lists/MyDocuments/Attachments/7/Slides_of_Coronavirus_for_non_virologists_talk_3_9_2020_1584455958.pdf)  
    
*   Ezekiel J, et al. "Fair Allocation of Scare Medical Resources in the Time of Covid-19", [The New England Journal of Medicine, March 24,2020](/efthymio/Lists/MyDocuments/Attachments/6/Fair%20Allocation%20of%20Scarce%20Medical%20Resources%20in%20the%20Time%20of%20Covid-19.pdf)  
    

  

*   thebmj - Covid-1:Asymptomatic cases may not be infectious, Wuhan study indicates [https://www.bmj.com/content/371/bmj.m4695](https://www.bmj.com/content/371/bmj.m4695)
