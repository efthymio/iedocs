
# Conferences - Workshops - Events of Interest


​A collection of interesting information in physics.  

Conferences
-----------

*   ​a conference  
    

Workshops
---------

*   ​P5 face-to-face meeting, November 2-4, 2013 @ FNAL -- [indico​](https://indico.fnal.gov/conferenceOtherViews.py?view=standard&confId=7485#all.detailed)   
    
*   Future Circular Collider Meeting, 12-15 February 2014 @ UniGe - [indico​](https://indico.cern.ch/event/282344/)  
    
*   ICFA Mini Workshop on Slow Extraction, Fermilab 22-24 July 2019 - [indico](https://indico.fnal.gov/event/20260/timetable/#20190722.detailed)  
    
*   Wright Colloquim Geneva - [2-6 November 2020](https://colloquewright.ch/fr/)  
    

CERN Accelerator Schools  
---------------------------

*   ​Beam Injection, Extraction and Transfer, Erice 2017 - [web](https://cas.web.cern.ch/schools/erice-2017)  
