
​​CV and Publications
=====================

  

*   My publications in [CERN CDS](https://cds.cern.ch/search?ln=en&sc=1&p=efthymiopoulos&action_search=Search&op1=a&m1=a&p1=&f1=&c=Articles+%26+Preprints&c=Books+%26+Proceedings&c=Presentations+%26+Talks&c=Periodicals+%26+Progress+Reports&c=Multimedia+%26+Outreach)  
    
*   My LHC-LUMI CDS [basket](https://cds.cern.ch/yourbaskets/display?category=P&topic=Default%20topic&bskid=2485&ln=en)  
    

  

  

  

*   LBNO Physics Potential, [http://arxiv.org/abs/1312.6520​](http://arxiv.org/abs/1312.6520)
