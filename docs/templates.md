​
# Document Templates
​
* XeLaTex file for EU Project deliverables - AIDA example: 
    - [AIDA-Del-DX.Y_v0_template.tex]](/templates/AIDA-Del-DX.Y_v0_template.tex)
    - [AIDALogo.png](/templates/AIDA-deliverable/AIDALogo.png)
    - [FP7Loog.png](/templates/FP7Logo.png)

* Instructions on A&T Sector Notes in CDS and LaTeX templates​

* from LATEX online - notes on book design
