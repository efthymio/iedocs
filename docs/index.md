# Ilias's Web

​​​​​​​​​​​​​​​​​​​​​Welcome!

I am a Particle and Accelerator Beam Physicist working at [CERN](http:www.cern.ch), presently as member of the Accelerators and Beam Physics [BE-ABP](https://be-dep-abp.web.cern.ch/) group within the [Beams Department](https://beams.web.cern.ch/). 

You can read more on my career and past activities in my [CV page](cv_and_publications.md)

-- to my old [web page](https://espace.cern.ch/efthymio/Wiki%20Pages/Home.aspx)

First-hand info
===============

*   Interesting web [links](mylinks.md)
    
*   Conferences - Workshops & Events of [interest](conferences_and_events.md)
    
*   Document [templates](templates.md)  
    
*   Scientific \[\[publications of interest\]\]

*   Links for [COVID-19](covid.md)   

*   Our World in Data - [web](https://ourworldindata.org/)  
    
My Projects and Research Activities

### LHC machine and experiments

*   Target Absorbers (TAS) and Target Absorber Neutral (TAN) for the LHC Experimental Areas  \[\[TAS-TAN\]\] - [new web](/lhc-tastan)  
    
*   Point-7 ventilation bypass  - [LHC-Point7​](/project-lhcls1-point7/SitePages/Home.aspx)
*   High Luminosity LHC :

*   [WP2](/HiLumi/WP2/Wiki/HL-LHC%20Parameters.aspx) Accelerator and Physics Performance - 
*   [WP8](/HiLumi/WP8/default.aspx) Machine Experiment Interface \[\[WP8MEI\]\]

*   Collection or interseting publications on LHC - \[\[LHCRef\]\]  
    
*   LHC Luminosity and Emittance - \[\[LHCLumi\]\]  
    
### Accelerator R&D projects

*   Machine parameters and projected luminosity performane of proposed future colliders at CERN - [pdf](http://cds.cern.ch/record/2645151/files/1810.13022.pdf)  
    
### Working Groups and Projects

*   PS-CSAP - [web](https://csap.web.cern.ch/ps-csap)  
    
*   LHC Background WG [indico](https://indico.cern.ch/category/2360/)
*   HL-LHC Experimental Data Quality WG - [EDQWG-indico](https://indico.cern.ch/category/7932/)  
    
*   LHC Emittance Preservation Working Group - [indico](https://indico.cern.ch/category/10421/)  
    
*   LHC Luminosity Calibration and Monitoring Working Group - [indico](https://indico.cern.ch/category/7484/)  
    
*   LHC Beam-beam and Luminosity Working Group - [indico](https://indico.cern.ch/category/4926/)  
    
*   Non-Linear beam dynamcis WG - [indico](https://indico.cern.ch/category/11612/)  
    
*   Run III Preparation WG - [indico](https://indico.cern.ch/category/10387/)  
    

Information on how to create such a Web site is found in [Documentation How-to Guide](https://how-to.docs.cern.ch/new/green/)

