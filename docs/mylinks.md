
# My Web links​

A collection of interesting web links.

Interesting Web pages within CERN
---------------------------------

*   AB Department: [ATB-SBA web](http://ab-div-atb-ea.web.cern.ch/ab-div-atb-ea/)
*   Statistics \[\[Statistics\]\]  
    
*   Non-LHC Physics workshop @ CERN - [indico agenda](http://indico.cern.ch/conferenceOtherViews.py?view=standard&confId=51128)
*   Check access privileges [ADAMS](http://access-control.web.cern.ch/access-control/ads/ads_page.htm)
*   CHIS and UNIQA web [portal](http://www.cern.ch/chis)
*   JACoW web page - [JACoW](http://www.jacow.org/)
*   CERN publication policy - [CDS notes](http://en-dep.web.cern.ch/en-dep/Publications/index.htm)
*   LTEX - [Home page](http://en-dep.web.cern.ch/en-dep/Groups/MEF/LTEX/index.htm)
*   Interactions News Wire - [web](http://www.interactions.org/)
*   PAF [web​](http://paf.web.cern.ch/paf/)
*   LHC Integration web [site​](http://project-integration-accelerateurs.web.cern.ch/project-Integration-Accelerateurs/frame_integration.htm)
*   Logging service wiki [site](https://wikis/display/CALS/CERN+Accelerator+Logging+Service)
*   DFS folder quota [web](http://cern.ch/go/DFSQuotaList)  
    

* AIS Presentations [web](http://ais.web.cern.ch/ais/presentations/)[​](http://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&ved=0CCUQFjAA&url=http://ais.web.cern.ch/ais/presentations/CET%2520Tutorial.ppt&ei=iZD0UsCoOaWBywPHlYLwCw&usg=AFQjCNFnOWbbdrzPi-V0WAsu0wM5RCR_Tw&bvm=bv.60799247%2cd.bGQ)

* Beam line contorl : [CESAR](http://slwww.cern.ch/~pcrops/releaseinfo/pcropsdist/cesar/cesar-gui/PRO/cesar-gui.jnlp), [XEMCGui](http://slwww.cern.ch/~pcrops/releaseinfo/pcropsdist/cesar/cesar-xemc/PRO/XemcGUI.jnlp)
*   FESA project : [cern.ch/project-fesa](http://project-fesa.web.cern.ch/project-fesa)
*   BE-CO wikis home page : [wikis.cern.ch](https://wikis.cern.ch/)  
    
*   operations logbook [e-logbook](https://ab-dep-op-elogbook.web.cern.ch/ab-dep-op-elogbook/)  
    
*   Impact - [web​](https://pptevm.cern.ch/impact/)
*   Organization of professional visits at CERN :

*   Etapes a suivre pour organiser des visites professionnelles dans les installations faisceau, Christelle GAINANT (BE-ASR) - [EDMS 1281098​](https://edms.cern.ch/file/1281098/2.1/Organisation_des_visites_professionnelles_dans_les_Beam_facilities_rev.2.1.pdf)
*   Registration of "professional visitors" for the CERN beam facilities, Rui NUNES (GS-ASE) - [EDMS 926814](https://edms.cern.ch/file/926814/5/20131203_Visitors_Procedure.pdf)
*   Formulaire enregistrement VISI - EDMS 926814 doc ([fr](https://edms.cern.ch/file/926814/5/formulaire_enregistrement_VISI.doc), [en​](https://edms.cern.ch/file/926814/5/formulaire_enregistrement_e_VISI.doc))
*   Professional visit request form - EDMS 926814 doc ([en/fr​](https://edms.cern.ch/file/926814/5/20080717_Visitors_Procedure_Annex_Formulaire.doc))  
    

Committees and WGs  
---------------------

*   LHC Comittee - [web](https://committees.web.cern.ch/lhcc)  
    

Physics Web sites of interest  
--------------------------------

*   US DoE [web​](http://science.energy.gov/hep/hepap/reports/)
*   US Particle Physics Prioritization Panel (P5) Report : HEPAP May 22, 2014 by [S. Ritz​](http://science.energy.gov/~/media/hep/hepap/pdf/May%202014/P5MayHEPAP-Ritz.pdf), and final report [document​](http://science.energy.gov/~/media/hep/hepap/pdf/May%202014/FINAL_DRAFT2_P5Report_WEB_052114.pdf)  
    
*   ETHZ Particle Physics Phenomenology - [HS2010](https://edu.itp.phys.ethz.ch/hs10/ppp1/)

Accelerator Beam Physics
-------------------------  

*   Accelerator [MD coordination](https://asm.cern.ch/asm?schedule=LHC%202018&version=1.5&state=Approved) - OP [web tools](https://op-webtools.web.cern.ch/) - [Statistics](https://acc-stats.web.cern.ch/acc-stats/#lhc/) - [e-logbook(good version!)](http://elogbook.cern.ch/)  
    
*   Accelerator Beam Doc  : [PSB](https://wikis.cern.ch/display/PSBOP/Beam+Docs+2018) - [PS](https://wikis.cern.ch/display/PSOP/Beam+Doc)   
    

*   Accelerator OP lectures - [indico](https://indico.cern.ch/category/8992/)  
    
*   The LHC beam production in CERN Injectors - Presentation by R. Steerenberg, February 2016 [pdf](http://be-op-lhc.web.cern.ch/sites/be-op-lhc.web.cern.ch/files/docs/ShutownLectures2016/LHC_BeamsProductionSchemes_2016_R-Steerenberg_Feb2016.pdf)  
      
    
*   ABP computing working group [wiki](https://twiki.cern.ch/twiki/bin/view/ABPComputing/WebHome)
*   PyJapc Scripting tools [wiki](https://wikis.cern.ch/display/ST/PyJapc)  
    
*   CERN Injectors Optics Repository - [web](http://cern-accelerators-optics.web.cern.ch/cern-accelerators-optics/) - [injoptics-web](https://project-injopt.web.cern.ch/)  
    